---
title: "Powell Sheagren's Portfolio"
---

## Table of Contents
1. [About Me](#about)
2. [Portfolio](#portfolio)
3. [News](#news)
4. [Resume](#resume)
5. [Contact](#contact)


## About Me: {#about}
Hello, I am a Student at Swarthmore College, class of 2022, who is studying Political Science and Statistics.

I specialized in using Statistical and Data Analytic techniques to analyze political data in topics ranging from International Welfare to Prison Education. I have utilized frequentist statistical methods and data visualization as key methods to help understand and process the world around us through data.

While at Swarthmore, I have also been very involved in the extracurricular performance arts scene where I have done dance and theater performances.

## Portfolio: {#portfolio}

The Following documents are a collection of knit RMarkdown Files, class project work, and samples of work I've done for professors. I will link to the document itself and give a brief summary of the context of the project. If there are any issues opening the documents please reach out to my contact information below.

### Global Fund Database Visualization:

Link: [Data Visualization](https://powellswarthmore.shinyapps.io/Sheagren-FinalProject/?_ga=2.53259368.1944537429.1611810522-2003757765.1611291487)

Summary: While taking a class on Data Visualization in January 2021, I returned to a data set I previously worked on, the public Global Fund Database, and created an interactive product which paired economic and demographic variables with basic Health System indicators. To make the interactive elements I utilized Shiny and Flexdashboard, two R packages.

### Automated Survey Data Cleaning:

Link: [ANES Data Cleaning](ANES_Merge.html)
Link: [ISSP Data Cleaning](ISSP_Merge.html)

Summary: Last summer I was working with Swarthmore Professor Ayse Kaya Orloff on a project comparing economic indicators of a country to its views on immigration and other measures. I utilized R to clean and combine various datasets, with the code shown above.

### Elections Multiple Linear Regression project:

Link: [Multiple Linear Regression](STATS-Final-Project.html)

Summary: During the Fall of 2020 I took a class on election politics and policy with Swarthmore Professor Carol Nackenoff for which we had a final project utilizing some given databases. I chose to look deeper into Legislative Participation and ran a multiple linear regression model to see how the identity of a federal congressperson effected the number of bills the sponsored or cosponsored.

### Class Data Visualization and Analysis:

Link: [Berlin AirBnB Analysis and Visualization](Final_Project_Report_WarVerSon.html)

Summary: In the Summer of 2021 as a part of a data science course, I worked with two other students on a final project analyzing Berlin AirBnB data. While the project was collaborative I coded all of the data visualizations and set up the MLR model, resulting in the final project which can be seen above.

### Happiness Index research:

Link: [Indexing Trial and Error](Happiness_Workshop--V2-.html)

Summary: Also in the Summer of 2021 I took part in a data methods workshop which was part learning part working with a 'client' on a project. The document contains my trial and error to find a proper method to index the data, as again I acted as programming lead on the team. We ended up using a Principal Component Analysis model and I continued working with the 'client' and the TA of our group on a paper which has been accepted to the 2022 WSRA spring conference.

## News: {#news}

### (12/27/2021) "The Principal Components of Happiness: The Mexican Case" Accepted to WRSA Conference

## Resume and CV: {#resume}
Resume: [Powell Sheagren's Resume](Powell_Sheagren_Resume.pdf)

## Contact {#contact}
Email: powell.sheagren@gmail.com

Phone: 760-330-3904

Address: 500 College Avenue, Swarthmore, PA, 19081

LinkedIn: [Powell Sheagren](https://www.linkedin.com/in/powell-reed-sheagren/)
